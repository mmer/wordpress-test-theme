<?php get_header(); ?>

<!-- Contenido de la página -->
  <div class="container pt-5">

    <div class="row pt-3">
      <!-- Columna de posts -->
      <div class="col-12 col-md-9">

        <!-- Post -->
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div class="card mb-4">
            <div class="card-body">
              <h2 class="card-title"><?php the_title(); ?></h2>
              <?php if (has_post_thumbnail() ){
                the_post_thumbnail('post-thumbnails', array(
                  'class' => 'img-fluid mb-3'
                ));
              } 
              ?>
              <p class="card-text"><?php the_excerpt(); ?></p>
              <p class="small"> Categorías: <?php the_category(' | '); ?></p>
              <a href="<?php the_permalink(); ?>" class="btn btn-primary">Leer más &rarr;</a>
            </div>
            <div class="card-footer text-muted">
                <?php echo get_the_date(); ?> - <?php the_author(); ?>
            </div>
          </div>       
        <?php endwhile; else : ?>
            <!-- Si no hay posts, devolvemos este mensaje -->
            <p>Lo siento, no hemos encontrado ningún post.</p> 
        <?php endif; ?>

        <!-- Pagination -->
        <div class="card-body">
          <?php get_template_part( 'template-parts/content', 'pagination'); ?> 
        </div>

      </div>

    <!-- Sidebar -->
    <?php get_sidebar(); ?>

    </div>
  </div>
  
<?php get_footer(); ?>
<?php get_header(); ?>

  <div class="container pt-5">
    <div class="row pt-3">

      <div class="col-lg-8">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- Título -->
        <h1 class="mt-4"><?php the_title(); ?></h1>
        <!-- Autor -->
        <p class="lead">
          Por 
          <?php the_author(); ?>
        </p>
        <hr>
        <!-- Fecha -->
        <p>Publicado <?php the_date();?> </p>
        <hr>
        <!-- Imagen -->
        <?php if (has_post_thumbnail() ){
          the_post_thumbnail('post-thumbnails', array(
            'class' => 'img-fluid mb-3'
            ));
          } 
        ?>
        <hr>
        <!-- Contenido del post -->
        <?php the_content(); ?>
        <hr>
        <!-- Categorías -->
        <p class="small"> Categorías: <?php the_category(' | '); ?></p>
        <hr>        
        <?php endwhile; else : ?>
            <p>Lo siento, no hemos encontrado ningún post.</p>
        <?php endif; ?>
      </div>
      <!-- Sidebar -->
      <?php get_sidebar(); ?>
    </div>
  </div>
<?php get_footer(); ?>
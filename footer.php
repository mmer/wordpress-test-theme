<footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; <?php bloginfo('name'); echo ' '.date('Y'); ?> </p>
    </div>
  </footer>
  <?php wp_footer(); ?>

</body>

</html>

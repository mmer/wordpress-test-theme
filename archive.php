<?php get_header(); ?>
<div class="container pt-5">
    <div class="row pt-3">
        <div class="col-12 col-md-9">

            <!-- Titulo de la página -->
            <h1 class="my-4"> <?php 
            //Verificamos que el archivo sea de fecha
            if(get_the_archive_title()){
                the_archive_title();
            }
            //Si nO es una fecha
            else{
                single_term_title();            
            } ?> 
            </h1>
            <p><?php echo term_description(); ?></p>

            <!-- Post -->
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <!-- Contenido del post --> 
                <div class="card mb-4">
                    <div class="card-body">
                        <h2 class="card-title"><?php the_title(); ?></h2>
                        <p class="card-text"><?php the_excerpt(); ?></p>
                        <p class="small"> Categorías: <?php the_category(' | '); ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Leer más &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                    <?php echo get_the_date(); ?> - <?php the_author(); ?>
                    </div>
                </div>

            <?php endwhile; else : ?>
            <p>Lo siento, no hemos encontrado ningún post.</p>
 
            <?php endif; ?>
           
            <!-- Pagination -->
            <div class="card-body">
                <?php get_template_part( 'template-parts/content', 'pagination'); ?> 
            </div>
        </div>

        <!-- Sidebar -->
        <?php get_sidebar(); ?>

    </div>
</div>

<?php get_footer(); ?>
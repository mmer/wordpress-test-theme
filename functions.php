<?php 

// Etiqueta de titulo 
add_theme_support( 'title-tag' );

// Estilos y JS
function mertheme_add_css_js() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/jquery/jquery.min.js', array(), '3.5', true );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.bundle.min.js', array('jquery'), '4.5', true );
}
add_action( 'wp_enqueue_scripts', 'mertheme_add_css_js');

function mertheme_menu_register() {
    register_nav_menu( 'menu-principal', 'Menu Principal' );
}
add_action( 'after_setup_theme', 'mertheme_menu_register' );
 
function mertheme_nav_class($classes, $item){
    $classes[] = 'nav-link';
    return $classes;
}
add_filter('nav_menu_css_class' , 'mertheme_nav_class' , 10 , 2);
 
function mertheme_sidebar_register() {
    register_sidebar( array(
        'name' =>'Sidebar',
        'id' => 'sidebar-mer',
        'description' => 'Barra lateral del tema Mer.',
        'before_widget' => '<div id="%1$s" class="card my-4 %2$s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h5 class="card-header">',
        'after_title'   => '</h5><div class="card-body">',
    ) );
}
add_action( 'widgets_init', 'mertheme_sidebar_register' );

// Imágenes destacadas
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
 }

